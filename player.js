
module.exports = {

  VERSION: "Default JavaScript folding player",


  bet_request: function(game_state, bet) {
  
  var highCard = [
    "J", "Q", "K", "A"  
  ];
  
  var cards = [
      "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"
  ];
  
  var goodHands = ["AAo","AKs","AQs","AJs","A10s","A9s","A8s","A7s","A6s","A5s","A4s","A3s","A2s","AKo","KKo","KQs","KJs","K10s","K9s","K8s","K7s","K6s","K5s","K4s","K3s","K2s","AQo","KQo","QQo","QJs","Q10s","Q9s","Q8s","Q7s","Q6s","Q5s","Q4s","Q3s","Q2s","AJo","KJo","QJo","JJo","J10s","J9s","J8s","J7s","J6s","J5s","J4s","J3s","J2s","A10o","K10o","Q10o","J10o","1010o","A9o","K9o","Q9o","J9o","99o","A8o","K8o","Q8o","J8o","88o","A7o","K7o","Q7o","J7o","77o","A6o","K6o","Q6o","66o","A5o","Q5o","55o","A4o","44o","A3o","K3o","33o","A2o","22o"];
   
      function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
    
    function isGoodHand(hand){
        return isInArray(hand, goodHands);
    }
    
    function numberOfActive(players){
        var db = players.length;
        for (var i=0; i<players.length; i++){
            if (players[i].status === "out"){
                db--;
            }
        }
        console.log("active players: " + db);
        return db;
    }
    
    function encodeHand(hand){
        var colour;
        var first;
        var second;
        if(cards.indexOf(hand[0].rank) > cards.indexOf(hand[1].rank)){
            first = hand[0].rank;
            second = hand[1].rank;
        }else{
            first = hand[1].rank;
            second = hand[0].rank;
        }
        if (hand[0].suit === hand[1].suit){
            colour = "s";
        }else{
            colour = "o";
        }
       
        return "" + first + second + colour;
    }
    var actualBet = 0;
    var buyIn = game_state.current_buy_in;
    var index = game_state.in_action;
    var myPlayer = game_state.players[index];
    var hand = myPlayer.hole_cards;
    var highCard = isInArray(hand[0].rank, highCard) || isInArray(hand[1].rank, highCard);
    var onePair = hand[0].rank === hand[1].rank;
    var encodedHand = encodeHand(hand);
    var goodHand = isGoodHand(encodedHand);
    
    console.log("hand is good? " + goodHand + ", hand : " + encodedHand);
    console.log("buyIn: " + buyIn);
    console.log("onepair: " + onePair + " highCard" + highCard);
    
    if (numberOfActive(game_state.players) <= 3){
        console.log("active players <= 3");
        if (goodHand){
            console.log("hand is good");
            actualBet = buyIn;
        }else{
            console.log("hand is bad");
            actualBet = 20;
        }
    }else{
        if (onePair && highCard){
            actualBet = 800;
            console.log("High pair!!!!");
        }else {
            console.log("Loosing hand.");
            actualBet = 0;
        }
    }
    console.log("actual bet: " + actualBet);
    bet(actualBet);
  },

  showdown: function(game_state) {

  }

};
